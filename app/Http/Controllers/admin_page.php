<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\courses_type;

class admin_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_page');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo '<pre>';
        print_r($request->all());
        $new_course_add_query = new courses_type();
        $new_course_add_query->name = $request->course_name;
        $new_course_add_query->description = $request->course_description;
        if($request->hasfile('course_image'))
        {
            $image = $request->file('course_image');
            $original_name = $image->getClientOriginalName();
            $set_random_name = rand().time().$original_name;
            echo $original_name.'<br>';
            $image->move(public_path().'/course_image/',$set_random_name);
            $new_course_add_query->image = $set_random_name;
            //echo  $set_random_name;
        }
        $new_course_add_query->save();
        return redirect('admin');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
