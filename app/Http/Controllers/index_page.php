<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\registration;
use App\courses_type;
use App\course_data;
class index_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $get_course_type = courses_type::latest('updated_at')->take(3)->get();
        //echo '<pre>';
        //print_r($get_course_type);
        //die();
        if(!Session::get('login_email'))
        {
            return view('index')->with('course_type_list',$get_course_type);
        }else{
            $get_user_type = registration::where('email',Session::get('login_email'))->first();
            if($get_user_type->user_type == 'user')
            {
                return view('index')->with('course_type_list',$get_course_type);
            }else{
                return redirect('instructor_dashboard');
            }
        }
       
       
        //echo $get_user_type;
       // if(!$get_user_type)
       // {
        //    echo 'yes';
       // }       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $get_course_data_from_requested_id = course_data::where('course_category',$id)->get();
        //echo '<pre>';
        //print_r($get_course_data_from_requested_id);
        return view('coursecategory')->with('course_data',$get_course_data_from_requested_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewsinglecourse($id)
    {
        //echo $id;
        //die();
        $get_single_course_data = course_data::where('id',$id)->first();
        $get_instructor_data = registration::where('email',$get_single_course_data->user_email)->first();
        //echo '<pre>';
        //print_r($get_single_course_data);
        //die();
        return view('viewmore')->with('single_course_data',$get_single_course_data)->with('instructor_data',$get_instructor_data);
    }
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
