<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\registration;
use App\veriify_email;

use Session;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
class registration_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('registration_page');
    }



    public function login_form(Request $request)
    {
        //echo '<pre>';
        //print_r($request->all());
        $login_check = registration::where('email',$request->email)->first();
        if(Hash::check($request->password,$login_check->password ))
        {  
            if($login_check->user_type == 'user')
            {
                Session::put('login_email',$login_check->email);
                Session::put('login_user_id',$login_check->id);
                Session::put('user_type',$login_check->user_type);
                return redirect('/');
            }else{
                Session::put('login_email',$login_check->email);
                Session::put('login_user_id',$login_check->id);
                Session::put('user_type',$login_check->user_type);
                return redirect('/instructor_dashboard');
            }
            
            //echo 'correct';
        }else{
            session()->flash('login_error','Your credentials are not correct');
            return redirect('/');
            echo 'uncorrect';
        }
        //print_r($login_check);

    }
    public function instructor_dashboard()
    {
        return view('instructordashboard');
    }


    public function logout()
    {
        Session::flush();
        return redirect('/');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
            'email' => 'required|email:rfc,dns',
            
        ]);
        //print_r($request->all());
       // die();
            
        $insert_data = new registration();
        
        $insert_data->email = $request->email;
        $number_of_digits = 30;
        $rand_number =  substr(number_format(time() * mt_rand(),0,'',''),0,$number_of_digits);
        $insert_data->provider_user_id =  $rand_number;

        $insert_data->save();
        session()->flash('register_complete','Please Complete your details');
        Session::put('google_email',$insert_data->email);
        Session::put('provider_user_id',$insert_data->provider_user_id);
        return redirect('registration_complete_view');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //echo '<pre>';
       // print_r($request->all());
        $token = Str::random(20);
        $set_email_verify = new veriify_email();
        $set_email_verify->provider_user_id = $request->provider_user_id;
        $set_email_verify->email = $request->email;
        $set_email_verify->token = $token;
        $set_email_verify->save();
        $mail_array = array('user_name'=>$request->name,'user_email'=>$request->email,'token'=>$token);
        Mail::send('send_email.verify_email_page',$mail_array,function($m) use ($mail_array){
            $m->to($mail_array['user_email'])->subject('This is your verification email');
            $m->from('TectAcademicAdmin@gmail.com','Verify Email');
        });

 
        //$set_email_verify->token = $token ;
        //print_r($set_email_verify);
       // die();
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $image->move(public_path().'/profile_image/',$name);
        }else{
            $name = 'default.jpg';
        }
        $update_data = registration::where('email',$request->email)->update(['name'=>$request->name,"password"=>Hash::make($request->password),'user_type'=>$request->user_type,'image'=>$name]);
        // update fields of registration table of particular email
        session()->flash('registration_done','Your Registration is done successfully. A verification link has been send to your email');
        Session()->flush();
        return redirect('/');
    }
     

    public function verify_email_fn($token)
    {
        //echo $token;
        $check_verify_email = veriify_email::where('token',$token)->first();
        //echo '<pre>';
        //print_r($check_verify_email);
        $registration_verify_done = registration::where('email',$check_verify_email->email)->where('provider_user_id',$check_verify_email->provider_user_id)->update(['email_verification'=>'verified']);
        $check_verify_email->delete();
        session()->flash('Verification done','Your verification is done ');
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
