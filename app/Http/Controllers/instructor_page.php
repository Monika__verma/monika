<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\course_data;
use App\courses_type;
use Session;
class instructor_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //echo '<pre>';
        //print_r($request->all());
        //die();
        $add_course = new course_data();
        $add_course->user_id = $request->user_id;
        $add_course->user_email= $request->user_email;
        $add_course->name = $request->title;
        $add_course->course_category = $request->category;
        $add_course->course_specification = implode(',',$request->case_specification) ;
        $add_course->course_requirement = implode(',',$request->course_requirement) ;
        $add_course->course_content = implode(',',$request->course_content) ;
        $add_course->course_description = $request->course_description;
        $add_course->course_price = $request->course_price;
        if($request->hasfile('course_video'))
        {
            foreach($request->file('course_video') as $video_file)
            {
                $video_name = $video_file->getClientOriginalName();
                $video_file->move(public_path().'/instructor_course_videos/',$video_name);
                $video_array[] = $video_name;
            }
        }
        if($request->hasfile('course_image'))
        {
            foreach($request->file('course_image') as $image_file)
            {
                $image_name = $image_file->getClientOriginalName();
                $image_file->move(public_path().'/instructor_course_images/',$image_name);
                $image_array[] =  $image_name;
            }
        }
        if($request->hasfile('course_notes'))
        {
            foreach($request->file('course_notes') as $notes_file)
            {
                $notes_name = $notes_file->getClientOriginalName();
                $notes_file->move(public_path().'/instructor_course_notes/',$notes_name);
                $notes_array[] = $notes_name;
            }
        }
        $add_course->image = implode(',',$image_array);
        $add_course->video = implode(',',$video_array);
        $add_course->document = implode(',',$notes_array);
        //print_r($add_course);
        $add_course->save();
        session()->flash('course_added','Your course added Successfully');
        return redirect('instructor_dashboard');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function course_create()
    {
        $get_all_courses = courses_type::all();
        return view('add_details')->with('courses_data',$get_all_courses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function show_courses()
    {
        $get_course_data_of_single_user = course_data::where('user_email',Session::get('login_email'))->get(); 
        return view('show_course')->with('instructor_course_data',$get_course_data_of_single_user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function check_title(Request $request)
    {
       // print_r($request->all());
        $check_title = course_data::where('name',$request->title)->exists();
        //return ($check_title);
        if(!$check_title)
        {
            return 'new_title';
        }else{
            return 'title_exists';
        }
        
    }
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
