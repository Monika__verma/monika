<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\registration;
use App\course_data;
use Session;
use App\cart_data;

class user_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo '<pre>';
        //print_r($request->all());

        $add_cart = cart_data::where('user_email', Session::get('login_email'))->first();
        // echo '<pre>';
        // echo 'checking cart:- ';
        // print_r($add_cart);
        // die();
        //this will save ur course data again and again whenever it's find that ur 
        // $add_cart is not empty and getting email
        if (!$add_cart) {
            $create_cart = new cart_data();
            $create_cart->user_id = Session::get('login_user_id');
            $create_cart->user_email = Session::get('login_email');
            $create_cart->instructor_email = $request->instructor_email;
            $create_cart->course_id = $request->course_id;
            $create_cart->course_name = $request->course_name;
            $create_cart->course_price = $request->course_price;
            //print_r($create_cart);
            $create_cart->save();
            return 'course_added';
        } else {
            if ($add_cart->course_id == $request->course_id) {
                return 'course already added';
            } else {
                $create_cart = new cart_data();
                $create_cart->user_id = Session::get('login_user_id');
                $create_cart->user_email = Session::get('login_email');
                $create_cart->instructor_email = $request->instructor_email;
                $create_cart->course_id = $request->course_id;
                $create_cart->course_name = $request->course_name;
                $create_cart->course_price = $request->course_price;
                $create_cart->save();

                // $add_cart->course_id = $add_cart->course_id.','.$request->course_id;
                // $add_cart->course_name = $add_cart->course_name.','.$request->course_name;
                // $add_cart->course_price = $add_cart->course_price.','.$request->course_price;
                // $add_cart->save();
                return 'course_added';
            }
        }
        //return($add_cart);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
