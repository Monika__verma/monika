<?php

namespace App\Http\Controllers;

use App\SocialAuthGoogleController;
use Illuminate\Http\Request;
use Socialite;
use Session;

use App\Services\SocialGoogleAccountService;


class SocialAuthGoogleControllerController extends Controller
{
    /**
   * Create a redirect method to google api.
   *
   * @return void
   */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

  /**
     * Return a callback method from google api.
     *
     * @return callback URL from google
     */
    public function callback(SocialGoogleAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->user()); 
        // echo '<pre>';
        // print_r($user);
        // echo 'registration name = '.$user->name;
        // die();
        if($user->name == '')
        {
            session()->flash('register_complete','Please Complete your details');
            Session::put('google_email',$user->email);
            Session::put('provider_user_id',$user->provider_user_id);
            return redirect('registration_complete_view');
        }
        else
        {
            // session()->flash('email_exists_google','Your Email is already registered with us. Please verify and Sign In with your email and password.');
            Session::flush();
            session()->flash('email_exists_google','Your Email is already registered with us.');
            return redirect('/');
        }
    }
}