<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\registration;
use Session;
use App\veriify_email;
use App\course_data;
class about_page extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $login_check = registration::where('email',Session::get('login_email'))->first();
        
        if(!$login_check)
        {
            Session::flush();
            session()->flash('login_required','Please login first to access the website');
            return redirect('/');
        }else{
            $verify_check = veriify_email::where('email',Session::get('login_email'))->exists();
            if(!$verify_check)
            {
                return view('aboutus');
            }else{
                session()->flash('Please_verify','You Need to verify first to access the website');
                return redirect('/');
            }
            
            
        }
        //die();
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function navbar()
    {
        $get_all_courses = course_data::all();
        return view('navbar_footer')->with('all_course',$get_all_courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
