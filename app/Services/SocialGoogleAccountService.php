<?php
namespace App\Services;
use App\SocialAuthGooglemodel;
use App\registration;
use Session;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialGoogleAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAuthGooglemodel::whereProvider('google')
            ->whereProviderUserId($providerUser->getId())
            ->first();
if ($account) {
            return $account->user;
        } else {
$account = new SocialAuthGooglemodel([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'google'  
            ]);
$user = registration::whereEmail($providerUser->getEmail())->first();
if (!$user) {
    $user = registration::create([
        'provider_user_id' => $providerUser->getId(),
        'email' => $providerUser->getEmail(),
        // 'name' => $providerUser->getName(),
        // 'password' => md5(rand(1,10000)),
        ]);
        
    }
    
    $account->user()->associate($user);
    $account->save();
return $user;
        }
    }
}