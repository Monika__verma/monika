<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>About Us</title>
    <link rel="stylesheet" href="../Techacademics/css/bootstrap.css">
    <script src="../Techacademics/jquery-3.4.1.js"></script>
    <script src="../Techacademics/js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Techacademics/fontawesome/css/all.css">
    <link rel="stylesheet" href="style/aboutus.css">
  
</head>
<!--aboutus css coding -->
<style>

    
/* .title {
    color: red;
    font-size: 52px;
    line-height: 1.13;
    margin-bottom: 15px;
    text-align: center;
    box-sizing: inherit;
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    font-size: 100%
}*/
    
    


    .btn1{
    margin-top: 45px;
    border: 1px solid transparent;
    background-image: linear-gradient(to left, #5c82ab 0%,#71bdaf 51%,#5c82ab 100%);
    display: inline-block;
    padding: 13.5px 45px;
    border-radius: 50px;
    font-size: 14px;
    border: 1px solid transparent;
}  
    .col-md-3:hover{
    border:1.5px solid #5c82ab;
  }
  .col-md-3:hover far fa-clone,fas fa-book-reader,far fa-bell{
    transform:scale(1.2);
  }
  .carousel-caption{
        position: absolute;
        top:50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

</style>

<body>
     <!--Menu Bar  -->
     <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
            <!--<iframe width="1345" height="367" src="https://www.youtube.com/embed/H-deFSyeXSM" frameborder="0" 
           allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->

           <video class="responsive-video" loop="" muted="true" style="height: 434px;   width: 1345px; z-index: 0;">
               <source src="https://cdn.kastatic.org/videos/homepage-background.mp4">
               <source src="https://cdn.kastatic.org/videos/homepage-background.webm">
               <source src="https://cdn.kastatic.org/videos/homepage-background.ogv">
   
</div>
</div>
</div>
     
     <div class="menu">
        <nav class="navbar navbar-expand-sm navbar-light p-fixed">
            <a href="#" class="navbar-brand text-white">Learning</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#hit">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="hit">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <form action="">
                            <input type="search">
                            <i class="fa fa-search"></i>
                        </form>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Courses</a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">IT</a>
                        <a href="#" class="dropdown-item">Science</a>
                        <a href="#" class="dropdown-item">Marketing</a>
                        <a href="#" class="dropdown-item">Health</a>
                        <a href="#" class="dropdown-item">Buisness</a>
                    </div>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Tutors</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Contact Us</a>
                    </li>
                    <button>Login</button>
                    <button>Signup</button>
                </ul>
            </div>
        </nav>
    </div>


    <!--aboutus-->
    


<div class="content">
    <div class="container-fluid">
        <div class="row mx-auto mt-5">
            <div class="col-md-2 bg-white pl-0 ml-4">
               <h1> Awesome Feature</h1>

               {{Session::get('login_email')}}
               {{Session::get('user_type')}}

               <p>Set have great you male grass yielding an yielding first their you're have called the abundantly fruit were man</p>
                <a href="#" class="btn1">Read More</a>
            </div>
            <div class="col-md-3  mx-auto text-center" >
                <i class="far fa-clone  mt-5 d-block "></i>
                      <h4  style="margin-bottom: 20px;margin-top:20px;" >Better Future</h4>
                        <p >Set have great you male grasses yielding yielding first their to called deep abundantly Set have great you male</p>
               </div>
            
            <div class="col-md-3 mx-auto text-center">
                <i class="fas fa-book-reader mt-5 d-block"></i>
                <h4 style="margin-bottom: 20px;margin-top:20px;" >Qualified Trainers</h4>
                <p>Set have great you male grasses yielding yielding first their to called deep abundantly Set have great you male</p>
             </div>
           
             <div class="col-md-3 mx-auto text-center ">
                <i class="far fa-bell   mt-5 d-block" ></i>
                <h4  style="margin-bottom: 20px;margin-top:20px;" >Job Oppurtunity</h4>
                <p >Set have great you male grasses yielding yielding first their to called deep abundantly Set have great you male</p>
            </div>
        </div>
    </div>
       
    </div>
    <div class="container-fluid">
       
        <marquee behavior='alternate'>
     
       
       <div class="row bg-secondary mt-5">

            <div class="col-md-2  pl-0 ml-2">
                <img src="about4.jpg" alt="" >
            </div>
            <div class="col-md-4 ">
                <img src="about.jpg" alt="">
            </div>
            <div class="col-md-2   ml-2">
                
                <img src="about2.jpg" alt="" >
            </div>
            <div class="col-md-2  ml-2">
                <img src="about1.jpg" alt="" >
            </div>
            
            <div class="col-md-1  ml-2">
                <img src="about3.jpg" alt="">
            </div></marquee>
            
        </div></div>
        <div class="container-fluid">
            <div class="row mx-auto mt-5 ">
                <div class="col-md-6 mx-auto">
                <h1 style="text-align: center;">Free tools for parents and teachers</h1>
                <p style="text-align: center;">We’re working hard to ensure that Khan Academy empowers coaches of all kinds to better
                understand what their children or students are up to and how best to help them. See at a glance whether
                a child or student is struggling or if she hit a streak and is now far ahead of the class. 
                Our teacher dashboard provides a summary of class performance as a whole as well as detailed student profiles.</p>
            </div>
        </div>
        </div>
<div class="container-fluid">
    <div class="row mt-5">
        <div class="col-md-12 mx-auto">
            <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
                <div class="collapse navbar-collapse">
                            <div class="carousel  slide border border-dark" data-ride="carousel" id="aaa">
                                
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="about5.jpg" width="1290" height="430" alt="">
                                        <div class="carousel-caption">
                                          
                                           <p style="font-size: x-large;">I just found your web site. I am 72 years old and I am now taking up learning where I left off. Thank you so much for all your hard work.
                                           </p>
                                           BARBARA
                                            </div>
                                            
                                        </div>
                                    
    
                                    <div class="carousel-item">
                                        <img src="about6.jpg" width="1290" height="430" alt="">
                                        <div class="carousel-caption">
                                            
                                            <p style="font-size: x-large;">Thank you for your ongoing inspiration. I am a primary school teacher in the north of England and when the conventional education system gets on my nerves, I only need to whack on a Khan Academy and my faith in the future of education is restored!
                                               </p> KIMBERLY</div>
                                    </div>
                                    <div class="carousel-item">
                                        <img src="about8.jpg"  width="1290" height="430" alt="">
                                        <div class="carousel-caption">
                                            
                                            <p style="font-size: x-large;">I just found your web site. I am 72 years old and I am now taking up learning where I left off. Thank you so much for all your hard work.
                                               </p> BARBARA
                                        </div>
                                    </div>
                                    </div>
                                    <a href="#aaa" class="carousel-control-prev" data-slide='prev'>
                                        <span class="carousel-control-prev-icon">
                                             </span>
                                    </a>
                                    <a href="#aaa" class="carousel-control-next" data-slide='next'>
                                        <span class="carousel-control-next-icon">
                                             </span>
                                    </a>
                                </div>
                            </div>
                            </nav>
                            </div>
                            </div>
</body>
</html>