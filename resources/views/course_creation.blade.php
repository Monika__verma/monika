<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Course</title>
    <link rel="stylesheet" href="../Techacademics/css/bootstrap.css">
    <script src="../Techacademics/jquery-3.4.1.js"></script>
    <script src="../Techacademics/js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Techacademics/fontawesome/css/all.css">
    <link rel="stylesheet" href="course_creation.css">
</head>
<body>
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-sm bg-light navbar-light fixed-top">
                    <a href="" class="navbar-brand">E-Learning</a>
                    <button class="navbar-toggler" data-target="#bcd" data-toggle="collapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="bcd">
                        <ul class="ml-auto">
                            <li class="nav-item"><a href="" class="nav-link">Exit</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
        <div class="container">
            <div class="row">
                <ul class="nav" id="pills-tab">
                    <li class="nav-item" id="tab-1">
                        <a class="nav-link active" data-toggle="tab" href="#first-tab">
                            Continue
                        </a>
                    </li>
                    <li class="nav-item" id="tab-2">
                        <a class="nav-link" data-toggle="tab" href="#second-tab">
                            Continue
                        </a>
                    </li>
                    <li class="nav-item" id="tab-3">
                        <a class="nav-link" data-toggle="tab" href="#third-tab">
                            Continue
                        </a>
                    </li>
                    <li class="nav-item" id="tab-4">
                        <a class="nav-link" data-toggle="tab" href="#fourth-tab">
                            Continue
                        </a>
                    </li>
                </ul>
                <!--section1-->
            <div id="first-tab" class="tab-pane fade show active">
                <div class="row mt-3 pt-5">
                    <div class="col-md-10 col-lg-10 mx-auto col1">
                        <h2 class="txt1">First, let's find out what type of course you're making.</h2>
                    </div>
                </div>
                <div class="row mt-2 pt-2">
                    <div class="col-md-4 col-lg-4 col-sm-12 mt-4  text-center col2">
                        <i class="fab fa-youtube icon1"></i>
                        <h4 class="mt-3">Course</h4>
                        <p class="mt-3 h5 txt2">Create rich learning <br>experiences with the help of<br> video
                            lectures,quizzes<br>coding,exercises.etc</p>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-12  col3 text-center mt-4 ">
                        <i class="far fa-newspaper icon1"></i>
                        <h4 class="mt-3">Practice Test</h4>
                        <p class="mt-3 h5 txt2">Help students prepare for<br>certification exams by<br>providing
                            practice<br>questions.</p>
                    </div>
                </div>
            </div>
            <!--section2-->
            <div id="second-tab" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-10 col-lg-10 mx-auto mt-4">
                        <h2 class="text-center">How about a working title?</h2>
                        <p class="text-center h5 mt-4 font-weight-normal">It's ok if you can't think of a good title now.
                            you can change it later.</p>
                        <form action="">
                            <input type="text" placeholder="e.g. Learn Photoshop CS6 from scratch"
                                class=" form-control form1 mt-5">
                        </form>
                    </div>
                </div>
            </div>
            <!--section3-->
            <div id="third-tab" class="tab-pane fade">
                <div class="row mt-5 pt-4">
                    <div class="col-md-10 col-lg-10 mx-auto mt-4">
                        <h2 class="text-center">What category best fit the knowledge you will share?</h2>
                        <p class="text-center h5 mt-4 font-weight-normal">If you are not sure about the right category, you
                            can change it later.</p>
                        <form action="">
                            <input type="text" placeholder="Choose a category" class=" form-control form1 mt-5">
                        </form>
                    </div>
                </div>
            </div>
            <!--section4-->
            <div id="fourth-tab" class="tab-pane fade">
                <div class="row mt-5 pt-4 mb-3 pb-5">
                    <div class="col-md-10 col-lg-10 mx-auto mt-4">
                        <h2 class="text-center">How much you can spend creating your course per week?</h2>
                        <p class="text-center h5 mt-4 font-weight-normal">There is no wrong answer. We can help you acheive
                            your goals even if you don't have much time.</p>
                        <form action="">
                            <div class="form-group group">
                                <input type="radio" class="check" name="radio1">
                                <p class="form-control input1">I'm very busy right now(0-2 hours)</p>
                            </div>
    
                            <div class="form-group group">
                                <input type="radio" class="check" name="radio1">
                                <p class="form-control input1">I'll work on this on the side(2-4 hours)</p>
                            </div>
    
                            <div class="form-group group">
                                <input type="radio" class="check" name="radio1">
                                <p class="form-control input1">I have lots of flexibility(5+ hours)</p>
                            </div>
    
                            <div class="form-group group">
                                <input type="radio" class="check" name="radio1">
                                <p class="form-control input1">I haven't yet decided if i have time</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!-- <ul class="nav nav-pills ml-auto">
            <li class="nav-item">
                <a data-toggle="tab" class="nav-link active" href="#second-tab">Continue</a>
    </li>
    </ul> -->
        <!--section5-->
        <!-- <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-sm bg-light navbar-light footer">

                    <ul class="ml-auto">
                        <form action="">
                            <button class="btn">Continue</button>
                        </form>
                    </ul>

                </nav>
            </div>
        </div> -->
</body>

</html>