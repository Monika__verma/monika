

  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signin</title>
    
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">

    <style>
.form .font-small {
    font-size: 1; }

.form .z-depth-1 {
    box-shadow: 0 2px 5px 0 rgba(14, 130, 231, 0.5), 0 4px 12px 0 rgb(185, 196, 230,0.5);
    box-shadow: 0 2px 5px 0 rgba(4, 104, 190, 0.5), 0 4px 12px 0 rgba(6, 59, 219, 0.5); }

.form .btn:hover {
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(11, 248, 63, 0.15);
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(5, 219, 76, 0.28); }

.form .modal-header {
    border-bottom: none; }
 

.form .modal-body, .form .modal-footer {
    font-weight: 400; }
    .blue-gradient {
        background: linear-gradient(360deg, #45cafc, #303f9f) !important;
    }
  .form-control{
      border: none;
      border-bottom: 1px solid grey;
  }


    </style>
</head>
<body >
<!-- Modal -->

<div class="modal fade" id="ModalForm" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">

    <!--Content-->

    <div class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
        @if($errors->any())
          <div class="alert alert-danger">
            <ul class="list-group">
              @foreach($errors->all() as $error)
                <li class="list-group-item">{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if(session()->has('register_complete'))
           <div>{{session()->get('register_complete')}}</div>
        @endif   
        <h3 class="modal-title w-100" id="myModalLabel"><strong>Complete your Registration</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="{{url('register_complete_form')}}" method="post" enctype='multipart/form-data'>
        @csrf()
        <div class="text-primary md-form mb-3">
            <label data-error="required"  for="name">Name</label>

            <input type="text" id="name" name="name" class="form-control validate bg-transparent"required>
        </div>

        <div class="text-primary md-form mb-3">
            <label data-error="required"  for="Form-email1">E-mail</label>
            @if(Session::has('google_email'))
            <input type="email" id="Form-email1" name="email" value='{{Session::get("google_email")}}' readonly class="form-control validate bg-transparent"required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
            
            <input type="hidden" id="Form-email1" name="provider_user_id" value='{{Session::get("provider_user_id")}}' readonly class="form-control validate bg-transparent"required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">

            @endif


        </div>

        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Password</label>

            <input type="password" id="Form-pass1" name="password" class="form-control validate  bg-transparent"required>
          
        </div>


        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> User Type</label>
                <select name="user_type" id="" class='form-control'>
                    <option value="user">User</option>
                    <option value="instructor">Instructor</option>
                </select>
        </div>


        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Profile Image</label>
            <input type="file" id="Form-pass1" name="image" class="form-control validate  bg-transparent"required>  
        </div>
        <p class="font-small text-dark d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1">
              Password?</a></p>
   

        <div class="text-center mb-3">
          <input type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1" name="submit" value="Login">
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or SignIn
          with:</p>

          <!--icon-->
          </form>
        
      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="#" class="blue-text ml-1">
            Sign Up</a></p>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!-- Modal -->

<div class="text-center">
  <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#ModalForm">
     Signin</a>
</div>

<script>
  $(function(){
    $('#ModalForm').modal('show');
  })

</script> 

</body>
</html>