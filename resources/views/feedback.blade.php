<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Feedback Form</title>
    <link rel="stylesheet" href="../Techacademics/css/bootstrap.css">
    <script src="../Techacademics/jquery-3.4.1.js"></script>
    <script src="../Techacademics/js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Techacademics/fontawesome/css/all.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto text-center">
                <h2 class="mt-4">FEEDBACK FORM</h1>
                    <p>We would love to hear your thoughts, concerns or problems with anything so we can improve!</p>
                    <hr>
                </div>
                <form action="">
                    <div class="col-md-10 mx-auto">
                        <h4>Feedback Type -:</h4>
                        <input type="radio" name="feeds" id="">Questions
                        <input type="radio" name="feeds" id="">Comments
                        <input type="radio" name="feeds" id="">Bugs
                    </div>
                    <div class="col-md-10 mx-auto">
                        <h4>Describe Feedback -:</h4>
                        <textarea name="" id="" cols="30" rows="10"></textarea>
                    </div>
                    <div class="col-md-10 mx-auto">
                        <h4>Full Name -:</h4>
                        <input type="text" name="fname" id="" placeholder="First Name">
                        <input type="text" name="lname" id="" placeholder="Last Name">
                    </div>
                    <div class="col-md-10 mx-auto">
                        <h4>E-mail Id -:</h4>
                        <input type="email" name="" id="">
                    </div>
                    <button>Submit Feedback</button>
                </form>
        </div>
    </div>
</body>

</html>