@extends('navbar_footer')

@section('navbar')

@parent

@endsection

@section('main_content')

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 page_heading">
          <h1 class="text-dark">My Courses</h1>
          <p class="text-dark">Courses</p>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="card">
            <img class="card-img-top" src="Images/web.jpg" mx-auto />
            <div class="card-body">
              <h6 class="card-title">
                <b>The complete 2020 WEb Development Course - Build ...</b>
              </h6>
              <p class="card-subtitle">
                Development Island (UK), Best Technology Courses on the...
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="card">
            <img class="card-img-top" src="Images/php.jpg" />
            <div class="card-body">
              <h6 class="card-title">
                <b>Practical PHP: Master the Basics and Code Dynamic...</b>
              </h6>
              <p class="card-subtitle">
                Brad Hussey, Marketing Consultant at...
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="card">
            <img class="card-img-top" src="Images/english.jpg" alt="english.jpg"/>
            <div class="card-body">
              <h6 class="card-title">
                <b>English Launch: Learn English for Free - Upgrade all areas</b>
              </h6>
              <p class="card-subtitle">
                Anthony Kelleher, Bachelor of Arts TESOL - English Teacher at...
              </p>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-3">
          <div class="card">
            <img class="card-img-top" src="Images/lara.png" />
            <div class="card-body">
              <h6 class="card-title">
                <b>Laravel for Beginners: Make Blog in Laravel 5.2</b>
              </h6>
              <p class="card-subtitle">
                Aadil Khan, Software Engineer
              </p>
            </div>
          </div>
        </div>
        

        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card">
              <img class="card-img-top" src="Images/ja.png" />
              <div class="card-body">
                <h6 class="card-title">
                  <b>Click Games Javscript - 2 Game projects from scratch</b>
                </h6>
                <p class="card-subtitle">
                  Laurence Svekis, Instructor, 18+yrs Web Experience
                </p>
              </div>
            </div>
          </div>
      </div>
    </div>
    @endsection



@section('footer')

@parent

@endsection

