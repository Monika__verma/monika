<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instructor Dashboard</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('style/instructordashboard.css')}}">
</head>

<body>
    <div class="sidenav">
        <div class="wrapper">
            <ul>
                <li>
                    <i class="fab fa-etsy ficon" aria-hidden="true"></i>
                    <div class="slider">
                        <a href="{{url('instructor_dashboard')}}">E-LEARNING</a>
                    </div>
                </li>
                <li>
                    <i class="fab fa-youtube ficon" aria-hidden="true"></i>
                    <div class="slider">
                        <a href="{{url('show_course')}}">View Course</a>
                    </div>
                </li>
                <li>
                    <i class="fas fa-envelope ficon" aria-hidden="true"></i>
                    <div class="slider">
                        <a href="">Communication</a>
                    </div>
                </li>
                <li>
                    <i class="fas fa-tools ficon" aria-hidden="true"></i>
                    <div class="slider">
                        <a href="">Tools</a>
                    </div>
                </li>
                <li>
                    <i class="far fa-question-circle ficon" aria-hidden="true"></i>
                    <div class="slider">
                        <a href="">Resources</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--section1-->
                <nav class="navbar navbar-expand navbar-light">
                    <div class="navbar-nav ml-auto">
                       <a href="mainpage.html" style="font-weight: bold;">STUDENT</a>
                       <a href="{{url('instructorprofile')}}" style="font-weight: bold;">PROFILE</a>
                        <p class="nav-item stu"> <i class="far fa-bell ml-3 icon1"></i></p>
                    </div>
                </nav>
            </div>
        </div>