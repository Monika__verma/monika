@extends('navbar_footer')
@section('navbar')

@parent

@endsection

<head>
<link rel="stylesheet" href="{{asset('style/coursecategory.css')}}">
<style>
        .main2 button {
            display: block;
        }
        
        .main2 h5,
        .main2 button {
            text-align: center;
        }
    </style>
</head>

@section('main_content')
    <!-- Decription of content -->
    
    
    <div class="container mt-5 ">
        <div class="row ">
            <div class="col-md-12 main " style="box-shadow: 5px 5px 5px black;">
                <h2><span class='text-capitalize'>{{request()->route('id')}}</span> Courses</h2>
                <p>Etrain range of free online IT training courses includes clear and simple lessons on how to develop software, manage computer networks, and maintain vital IT systems across computers and phones. In today's digital world, these pieces of
                    technology facilitate almost everything we do in our personal and professional lives. This means that IT professionals are some of the most valued employees today.By studying up, you can join their ranks.</p>
            </div>
        </div>
    </div>
    <!-- Info of courses -->
    <div class="container-fluid ">
        <div class="row main2 ">
            <div class="col-md-12 ">
            @if(count($course_data) == 0)
                <h2>Right now there is no course in thiscategory. Please come back later.</h2>
            </div>
            @else
                <h2>Top {{count($course_data)}} <span class='text-capitalize'>{{request()->route('id')}}</span> Courses</h2>
            </div>
            @foreach($course_data as $course)
            
            @php
                $image = explode(',',$course->image);
                $img = $image[0];             

            @endphp
            <div class="col-sm-6 col-md-6 col-lg-4">
            <img src='{{asset("instructor_course_images/$img")}}' class="d-block mx-auto">

                <h5>{{$course->name}}</h5>
                <a href="{{url('viewmore',[$course->id])}}"><button class="btn btn-outline-dark">Start Now</button></a>
                </div>
            @endforeach
            @endif
          <!--  <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p6.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p5.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p1.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p2.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p4.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p3.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p3.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p6.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p2.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p5.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p4.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <img src="Images/p2.jpg " class="d-block mx-auto">
                <h5>Introduction of Information Technology</h5>
                <button class="btn btn-outline-dark ">Start Now</button>
            </div>-->
        </div>
    </div>
    
    @endsection

@section('footer')

@parent

@endsection
