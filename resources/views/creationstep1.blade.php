<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>First</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('style/creationstep1.css')}}">
    <style>
        .group {
            position: relative;
        }

        .check {
            position: absolute;
            left: 15%;
            top: 33%;
        }

        .input1 {
            width: 72% !important;
            margin-left: 14%;
            padding-left: 25px;
        }
        .tab-content
        {
            background-color: #f2f3f5;
        }
        a
        {
            text-decoration: none;
            color: white;
        }
    </style>F
</head>

<body>
    <div class="container-fluid px-0">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-sm bg-light navbar-light fixed-top">
                    <a href="" class="navbar-brand">E-Learning</a>
                    <button class="navbar-toggler" data-target="#bcd" data-toggle="collapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="bcd">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item"><a href="{{url('instructor_dashboard')}}" class="nav-link">Exit</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <!-- Tabs -->
    <div class="container-fluid">
        <form action="" method="post" id="create_course" novalidate>
            <div class="row mt-5">
                <div class="col-md-12 col-sm-12 col-12 px-0">
                    <div class="tabs bg-info">
                        <ul class="nav nav-tabs  text-center" role="tablist">
                            <li class="nav-item">
                                <a data-toggle="tab" href="#tab1" role="tab" class="active_tab" id="step1">
                                    <h6 class="text-white">Step 1</h6>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a data-toggle="tab" href="#tab2" role="tab" class="inactive_tab" id="step2">
                                    <h6 class="text-white">Step 2</h6>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a data-toggle="tab" href="#tab3" role="tab" class="inactive_tab" id="step3">
                                    <h6 class="text-white">Step 3</h6>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 px-0">
                    <div class="tab-content p-4">
                        <div id="tab1" class="tab-pane fade show active ml-3" role="tabpanel">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-10 col-lg-10 mx-auto mt-4">
                                        <h2 class="text-center">How about a working title?</h2>
                                        <p class="text-center h5 mt-4 font-weight-normal">It's ok if you can't think of a good title now. you can change it later.</p>
                                
                                            <input type="text" name='title' placeholder="e.g. Learn Photoshop CS6 from scratch" class=" form-control form1 mt-5" id="title">

                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <nav class="navbar">
                                                <ul class="ml-auto">
                                                <a data-toggle="tab" href="#tab2" role="tab" class="active_tab" id="step2">
                                                    <h6 class="text-white"><button class="btn btn-info d-block">Continue</button></h6>
                                                </a>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="tab2" class="tab-pane fade" role="tabpanel">

                            <div class="container">
                                <div class="row mt-5 pt-4">
                                    <div class="col-md-10 col-lg-10 mx-auto mt-4">
                                        <h2 class="text-center">What category best fit the knowledge you will share?</h2>
                                        <p class="text-center h5 mt-4 font-weight-normal">If you are not sure about the right category, you can change it later.</p>
                                            <select id="category" name='category' class="form-control">
                                                <option>IT</option>
                                                <option>Science</option>
                                                <option>Buisness and development</option>
                                                <option>Marketing</option>
                                                <option>Medical Science</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <nav class="navbar">
                                                <ul class="mr-auto">
                                                <a data-toggle="tab" href="#tab1" role="tab" class="inactive_tab" id="step1">
                                                    <h6 class="text-white"> <button class="btn btn-info d-block">Previous</button></h6>
                                                </a>
                                                </ul>
                                                    <ul class="ml-auto">
                                                    <a data-toggle="tab" href="#tab3" role="tab" class="inactive_tab" id="step3">
                                                        <h6 class="text-white"><button class="btn btn-info d-block">Continue</button></h6>
                                                    </a>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="tab3" class="tab-pane fade" role="tabpanel">
                            <div class="container">
                                <div class="container-fluid px-0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <nav class="navbar navbar-expand-sm bg-light navbar-light fixed-top">
                                                <a href="" class="navbar-brand">E-Learning</a>
                                                <button class="navbar-toggler" data-target="#bcd" data-toggle="collapse">
                                                    <span class="navbar-toggler-icon"></span>
                                                </button>
                                                <div class="collapse navbar-collapse" id="bcd">
                                                    <ul class="ml-auto">
                                                        <li class="nav-item"><a href="" class="nav-link">Exit</a></li>
                                                    </ul>
                                                </div>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <div class="row mt-5 pt-4 mb-3 pb-5">
                                        <div class="col-md-10 col-lg-10 mx-auto mt-4">
                                            <h2 class="text-center">How much you can spend creating your course per week?</h2>
                                            <p class="text-center h5 mt-4 font-weight-normal">There is no wrong answer. We can help you acheive your goals even if you don't have much time.</p>

                                                <div class="form-group group">
                                                    <input type="radio" class="check" name="radio1">
                                                    <input type="text" placeholder="I'm very busy right now(0-2 hours)" class=" form-control input1 mt-5" disabled>
                                                </div>

                                                <div class="form-group group">
                                                    <input type="radio" class="check" name="radio1">
                                                    <input type="text" placeholder="I'll work on this on the side(2-4 hours)" class=" form-control input1 mt-4" disabled>
                                                </div>

                                                <div class="form-group group">
                                                    <input type="radio" class="check" name="radio1">
                                                    <input type="text" placeholder="I have lots of flexibility(5+ hours)" class=" form-control input1 mt-4" disabled>
                                                </div>

                                                <div class="form-group group">
                                                    <input type="radio" class="check" name="radio1">
                                                    <input type="text" placeholder="I haven't yet decided if i have time" class=" form-control input1 mt-4" disabled>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <nav class="navbar">
                                                    <ul class="mr-auto">
                                                    <a data-toggle="tab" href="#tab2" role="tab" class="inactive_tab" id="step2">
                                                        <h6 class="text-white"> <button class="btn btn-info d-block">Previous</button></h6>
                                                    </a>
                                                    </ul>
                                                    <ul class="ml-auto">
                                                            <button class="btn btn-info d-block" id="two">
                                                            <a href="#" onclick='create_course_form()' >Submit</a>
                                                            </button>
                                                        </form>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>

        function create_course_form()
        {
         
            let title = $('#title').val();
            let category = $('#category').val();
            // alert(title);
            // alert(category);
            $.ajax({
                url:'{{url("create_course_form")}}',
                type:'get',
                data:{title:title,category:category},
                success:function(data)
                {
                    alert(data);
                }
            })
        }

    </script>
</body>
</html>
