@extends('navbar_footer')

@section('navbar')

@parent

@endsection

@section('main_content')

    <div class="container-fluid ">
        <div class="row">
            <!-- div for first section-->
            <div class="col-md-12 bg-dark" id="first-section">
                <h2 class="text-white">Shopping Cart</h2>
            </div>
            <!-- div for second section(cart images)-->
            <div class="col-md-7 mx-auto" id="cart">
                <p>Courses in Cart</p>
                <div class="col-md-12 text-center" id="cart-inner">
                    <i class="fas fa-shopping-cart " id="icon"></i>
                    <p class="para">Your Cart is empty.Keep shopping to find a course!</p>
                    <button class="btn btn-info d-block mx-auto" id="keep_button">Keep shopping</button>

                </div>
            </div>
        </div>
        <!-- div for third section(cart course)-->
        <!-- left section-->
           <div class="row" id="third-section">
            <div class="col-md-7 mx-auto" id="cart-course">
                <p class="course">1 Course in Cart</p>
                <div class="row">
                <div class="col-md-5" id="first">
                    <h6>20 Web Projects With Vanila script</h6>
                </div>
                <div class="col-md-3" id="second">
                    <h6>Remove Save for Later Move to Wishlist</h6>
                </div>
                <div class="col-md-4" id="third">
                    <p>Total:</p>
                    <h6>420</h6>    
                </div>
            </div>
                
            </div>
            <!-- right section(for price)-->
            <div class="col-md-5">
                <p>Total:</p>
                <h4>Rs:- 420</h4>
                <button class="btn btn-info" id="check_box">Checkout</button><br>
                
            </div>
        </div>
        </div>    

        @endsection



@section('footer')

@parent

@endsection