<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Demo Page</title>
    <link rel="stylesheet" href="../Techacademics/css/bootstrap.css">
    <script src="../Techacademics/jquery-3.4.1.js"></script>
    <script src="../Techacademics/js/bootstrap.js"></script>
    <link rel="stylesheet" href="../Techacademics/fontawesome/css/all.css">
    <link rel="stylesheet" href="demotry.css">
</head>
<body>
     <!--menu bar-->
     <div class="menu">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a href="mainpage.html" class="navbar-brand text-white">E-Learning</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#hit">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="hit">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item">
                        <a href="mainpage.html" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <div class="search-bar">
                            <form action="">
                                <input type="text">
                                <i class="fa fa-search search-icon"></i>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Courses</a>
                        <div class="dropdown-menu">
                            <a href="it.html" class="dropdown-item">IT</a>
                            <a href="#" class="dropdown-item">Science</a>
                            <a href="#" class="dropdown-item">Marketing</a>
                            <a href="#" class="dropdown-item">Health</a>
                            <a href="#" class="dropdown-item">Buisness</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Tutors</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="contact_us.html" class="nav-link">Contact Us</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto login">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Login</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Sign Up</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--ist section-->
    <div class="container-fluid image">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>GET A FREE DEMO</h2>
                <h5>Build your academic knowledge</h5>
                <button class="btn text-white">Read More</button>
            </div>
        </div>
        </div>

<!--second section-->
<div class="container-fluid sec2">
<div class="container bg-light mt-2 pt-2 mb-2 roww">
    <div class="row text-center pb-2">
        <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="card col1">
                <img src="Images/boy.jpg" alt="" class="card-img-top crdimg1">
                <div class="card-body">
                    <p class="card-title">Web Designing</p>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="card col2">
                <img src="Images/gred.jpg" alt="" class="card-img-top crdimg2">
                <div class="card-body">
                    <p class="card-title">Software Engineering</p>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="card col3">
                <img src="Images/leaf.jpg" alt="" class="card-img-top crdimg3">
                <div class="card-body">
                    <p class="card-title">Cloud Computing</p>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="card col4">
                <img src="Images/shad.jpg" alt="" class="card-img-top crdimg4">
                <div class="card-body">
                    <p class="card-title">Python</p>
                </div>
            </div>
        </div>

    </div>
  </div>
</div>

<!--section3-->
<div class="container-fluid sec3 pt-5 pb-5">
    <div class="container bg-light mt-3 mb-3">
    <div class="row mx-auto">
        <div class="col-md-8 mx-auto">
            <h3 class="mt-2">In your demo you will:</h3> 
            <p class="mt-4">1) See how to,produce manage and measure learning more effectively with elearning. </p>   
            <p>2) Explore in detail the features that will have the greatest impact on your team's goals. </p>
            <p>3) Learn about the services and support availabe to ensure your excel with elearning.</p>
            <p>4) Explore in details the features that will have the greatest impact on your teams goals.</p>
            <p>5) Learn about the services and support available to ensure you excel with elearning.</p>
        </div>
    </div>
    </div>
</div>
 
<!--section4-->
<div class="container-fluid sec4 mt-5">
    <div class="row mx-auto pt-5">
        <div class="col-md-4 mx-auto pt-3 pb-3">
            <form action=" " class="text-center form1">
                <h3 class="txt">BOOK A DEMO</h3>
                <input type="text" class="form-control mt-5" placeholder="first name*">
                <input type="text" class="form-control mt-5" placeholder="last name*">
                <input type="email" class="form-control mt-5" placeholder=" work email*">
                <input type="text" class="form-control mt-5" placeholder="self number*">
                <button class="form-control btn mt-5 btn-dark text-dark button1">Request for demo</button>
            </form>
        </div>
    </div>
</div>

<!--section5-->
<div class="container-fluid">
    <div class="container">
        <div class="row mx-auto">
            <div class="col-md-12 mx-auto">
             
            </div>
        </div>
    </div>
</div>

<!--footer-->
<footer class="footer container-fluid mt-5">
    <div class="upper-footer">
        <div class="row">
            <div class="col-md-3 mx-auto mt-4">
                <p>Our mission is to provide a free, world-class education to anyone,anywhere.</p>
                <img src="bby.jpg" alt="" class="img-fluid d-block img mb-2">

            </div>
            <div class="col-md-3 mx-auto mt-4">
                <p class="font-weight-bold">ABOUT</p>
                <p><a>News</a></p>
                <p><a>Impact</a></p>
                <p><a>Our team</a></p>
                <p><a>Our leadership</a></p>
                <p><a>Careers</a></p>
                <p><a>Internship</a></p>
            </div>
            <div class="col-md-3 mt-4">
                <p class="font-weight-bold">CONTACT US</p>
                <p><span class="font-weight-bold">Address :</span> Hath of it fly signs bear be one blessed after</p>
                <p><a>Help center</a></p>
                <p><a>Share your story</a></p>

                <p><a><span class="font-weight-bold">Phone :</span> 600540987</a></p>
                <p><a><span class="font-weight-bold">Email :</span>info@learning.com </a></p>
            </div>
        </div>
    </div>
    <hr>
    <div class="icons">
        <div class="row">
            <div class="col-md-6 list mx-auto">
                <ul>
                    <li>Terms of use</li>
                    <li class="ml-5">Privacy Policy</li>
                </ul>
            </div>
            <div class="col-md-6">
                <i class="fab fa-facebook"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-instagram"></i>
            </div>
        </div>
    </div>
    <hr>
    <div class="lower-footer text-center">
        Copyright @E-Learning All rights reserved
    </div>
</footer>

</body>
</html>