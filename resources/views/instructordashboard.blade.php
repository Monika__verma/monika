<x-instructornavbar/>
        <!--section2-->
        <div class="container sec1 mt-4">
            <div class="row mt-5 mb-5">
            @if(session()->has('course_added'))
            <div class="alert alert-success">{{session()->get('course_added')}}</div>
            @endif
                <div class="col-md-8">
                    <i class="far fa-address-card ml-2 icon2"></i>
                    <span class="h2 ml-2">Jump Into Course Creation</span>
                </div>

                <div class="col-md-4 mt-5">
                    <button class="btn btn-info form-control"> <a href="{{url('course_create')}}" style='color:white'>Create your course</a></button>
                </div>
            </div>

            <!--section3-->
            <div class="row">
                <div class="col-md-12 mt-5 mb-5">
                    <p class="h5 text-center txt1">Based on your experience, we think these resources will be helpful.
                    </p>
                </div>
            </div>
        </div>
        <!--section4-->
        <div class="container sec4">
            <div class="row">
                <div class="col-lg-4 col-md-6 mx-auto">
                    <img src="Images/dash.jpg" alt="" class="mt-2 mb-2 ml-3 img">
                </div>

                <div class="col-lg-8 mt-3">
                    <p class="h4 mt-5">Creating an Engaging Course</p><br>
                    <p class="txt">Whether you've been teaching for years or are teaching for the first time, you can
                        make
                        an engaging course. We've compiled resources and best practices to help you get to the next
                        level, no matter where you're starting.
                    </p>
                </div>
            </div>
        </div>

        <!--section5-->
        <div class="container">
            <div class="row row1 mt-5">
                <div class="col-md-6 mt-2 mb-2 col">
                    <img src="Images/music.jpg" alt="" class="image1">
                    <span class="h4 ml-2 span1">Get Started with Video </span><br>
                    <span class="h6 span2 mr-2">Quality video lectures can set your course apart.Use our resources to
                        learn basics.</span>
                    <button class="btn bg-transparent btn1">Get Started</button>
                </div>

                <div class="col-md-6  mt-2 mb-2 col1">
                    <img src="Images/grldash.jpg" alt="" class="image2">
                    <span class="h4 ml-2 span1">Build Your Audience</span><br>
                    <span class="h6 span2 mr-2">Set your course up for success by building your audience.</span>
                    <button class="btn bg-transparent btn2">Get Started</button>
                </div>
            </div>
        </div>

        <!--section6-->
        <div class="container sec6 mt-5">
            <div class="row">
                <div class="col-lg-4 col-md-6 mx-auto">
                    <img src="Images/clock.jpg" alt="" class="mt-5 mb-4 ml-3 img1">
                </div>

                <div class="col-lg-8  mt-3">
                    <p class="h4 mt-5">Join the Newcomer Challenge</p><br>
                    <p class="txt">Whether you've been teaching for years or are teaching for the first time, you can
                        make
                        an engaging course. We've compiled resources and best practices to help you get to the next
                        level, no matter where you're starting.
                    </p>
                    <button class="btn bg-transparent mb-5">Get Started</button>
                </div>
            </div>
        </div>

        <!--section7-->
        <div class="row">
            <div class="col-md-12 mt-5 mb-5">
                <p class="h5 text-center txt1">Have questions? Here are our most popular instructor resources.</p>
            </div>
        </div>

        <!--section8-->
        <div class="container">
            <div class="row mt-2">
                <div class="col-md-3  text-center">
                    <i class="far fa-file-video" style="font-size:35px;"></i>
                    <p class="h6">Test Video</p><br>
                    <p class="h6 txt3">Send us a sample video and get expert feedback.</p>

                </div>
                <div class="col-md-3 text-center">
                    <i class="far fa-address-book" style="font-size:35px;"></i>
                    <p class="h6">Teaching Center</p><br>
                    <p class="h6 txt3">Ask experienced instructors for advice, or browse answwer.</p>
                </div>
                <div class="col-md-3 text-center">
                    <i class="fas fa-apple-alt" style="font-size:35px;"></i>
                    <p class="h6">Insight Courses</p><br>
                    <p class="h6 txt3">Get guided by video courses for E-Learning instructors.</p>
                </div>
                <div class="col-md-3 text-center">
                    <i class="far fa-smile" style="font-size:35px;"></i>
                    <p class="h6">Help and Support</p><br>
                    <p class="h6 txt3">Browse our help center or contact our support team.</p>
                </div>
            </div>
        </div>

        <!--section9-->
        <div class="row pt-5 mt-3 pb-5">
            <div class="col-md-12 text-center mt-5 mb-5">
                <p class="h3" style="font-weight: normal;">Are You Ready to Begin ?</p>
                <button class="btn btn-info mt-3" style="width: 24%;">Create Your Course</button>
            </div>
        </div>
    </div>
</body>

</html>