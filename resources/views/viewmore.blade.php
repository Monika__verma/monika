@extends('navbar_footer')
@section('navbar')

@parent

@endsection

<head>
    <link rel="stylesheet" href="{{asset('style/viewmore.css')}}">
    <style>
        #already_in_cart {
            display: none;
        }
    </style>
</head>

@section('main_content')


<!--section2-->
@php
$date = explode(' ',$single_course_data->updated_at);
$specification = explode(',',$single_course_data->course_specification);
array_pop($specification);
$requirement = explode(',',$single_course_data->course_requirement);
$video = explode(',',$single_course_data->video);

array_pop($requirement);


@endphp

<div class="container-fluid">
    <div class="row row1 text-white">
        <div class="col-md-6 ml-5 mt-5 sec1">
            <span class="font-weight-bold h5">Development</span>
            <span class="ml-3">></span>
            <span class="ml-3">{{$single_course_data->course_category}} </span>
            <span class="ml-3">></span>
            <span class="ml-3">{{$single_course_data->name}}</span>
        </div>
        <div class="col-md-8 col1 mx-auto mt-2 mb-2">
            <h3>{{$single_course_data->name}}</h3>
            <p class="h6 font-weight-normal">Create a realworld backend for bootcamp directory</p>
            <p class="h6 font-weight-normal"><span class="badge badge-dark badge-pill">HIGHEST RATED</span>
                <i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i>
                <i class="fas fa-star icon ml-2"></i><i class="fas fa-star icon ml-2"></i> 6,939 students enrolled</p>

            <p class='text-capitalize'>Created by {{$instructor_data->name}}</p>

            <p><i class="fas fa-cog mr-2"></i>Last updated {{$date[0]}}</p>
            <p><i class="fas fa-globe mr-2"></i>English</p>
        </div>
    </div>
</div>
<!--section3-->
<div class="container mt-2 mb-4">
    <div class="row">
        <div class="col-md-8 col2">
            <p class="h5 mt-2 mb-2">What You,ll Learn</p>
            @foreach($specification as $spec)
            <p><i class="fas fa-check mr-2 h6"></i>{{$spec}}</p>
            @endforeach
            <!--<p><i class="fas fa-check mr-2 h6"></i>Expressed & Mongoose Middleware (Geocoding,Auth,Error Handling, etc)</p>
                    <p><i class="fas fa-check mr-2 h6"></i> API Documentation & Deployment</p>
                    <p><i class="fas fa-check mr-2 h6"></i> HTTP Fundamentals(Req/Res Cycle,Status Codes,etc)</p>
                    <p><i class="fas fa-check mr-2 h6"></i>JWT/Cookie Authentication</p>
                    <p><i class="fas fa-check mr-2 h6"></i> API Security</p>-->
            <p class="h4 mt-4">Requirements</p>
            @foreach($requirement as $req)
            <p class="h6 font-weight-normal"><i class="fas fa-circle mr-2 icon2"></i>{{$req}}</p>
            @endforeach

            <p class="h4 mt-5 pt-3">{{$single_course_data->course_description}}</p>
        </div>
        <div class="col-md-4 col-lg-4 col3  mx-auto">
            <div class="card">
                <iframe width="288" height="200" src='{{asset("instructor_course_videos/$video[0]")}}' style='width:100%' frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="card-body">
                    <h4 class="card-title"><i class="fas fa-rupee-sign"></i>{{$single_course_data->course_price}}</h4>
                    <form style='display:none'>
                        <input type="text" value='{{$single_course_data->user_email}}' name='instructor_email'>
                        <input type="text" value='{{$single_course_data->id}}' name='course_id'>
                        <input type="text" value='{{$single_course_data->name}}' name='course_name'>
                        <input type="text" value='{{$single_course_data->course_price}}' name='course_price'>


                    </form>
                    <p class="card-text">
                        @if(Session::get('login_email'))
                        <!-- course is not addes yet -->
                        <button class="btn btn1 form-control mt-2" id='submit_cart_form'>ADD TO CART</button>

                        <!-- course is already added -->
                        <button class="btn btn1 form-control mt-2" id='already_in_cart'>Already in Cart</button>



                        <button class="btn btn2 btn-outline-dark form-control mt-2">BUY NOW</button>
                        @else
                        <a href="{{url('/')}}"><button class="btn btn2 btn-outline-dark form-control mt-2">Please login</button></a>
                        @endif

                    </p>
                    <p class="mt-2 text-center">30-Day Money Back Guarantee</p>
                    <p class="h5 mt-3">This course includes</p>
                    <p class="mt-2"><i class="fas fa-file-video mr-2"></i>12 hours on demand video</p>
                    <p><i class="fas fa-circle mr-2"></i>Full lifetime access</p>
                    <p><i class="fas fa-mobile-alt mr-2"></i>Access on mobile</p>
                    <p><i class="fas fa-certificate mr-2"></i>Certificate of completion</p>
                    <hr>
                    <a href=""><i class="fas fa-share mr-2 icon1"></i>Share</a>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>
<!--section4-->
<div class="container">
    <div class="row">
        <div class="col-md-8">





        </div>

    </div>
</div>
<script>
    $(function() {
        $('#submit_cart_form').bind('click', function(event) {
            event.preventDefault();
            $.ajax({
                url: '{{url("add_to_cart")}}',
                type: 'post',
                data: $('form').serialize(),
                success: function(data) {
                    alert(data);

                    // this will active when course is addes -> there is not such a condition where u can check that course is already in cart or not !
                    // if (data == 'course_added') {
                    //     $('#submit_cart_form').css('display', 'none');
                    //     $('#already_in_cart').css('display', 'block');
                    // }
                }
            })
        })
    })
    //http:://add_to_cart?course_name = dfdd&course_id = fdf&------
</script>
@endsection

@section('footer')

@parent

@endsection