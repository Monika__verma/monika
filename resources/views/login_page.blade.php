


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="jquery-3.4.1.js"></script>
    <script src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="fontawesome/css/all.css">
  



    <style>
.form .font-small {
    font-size: 1; }

.form .z-depth-1 {
    box-shadow: 0 2px 5px 0 rgba(14, 130, 231, 0.5), 0 4px 12px 0 rgb(185, 196, 230,0.5);
    box-shadow: 0 2px 5px 0 rgba(4, 104, 190, 0.5), 0 4px 12px 0 rgba(6, 59, 219, 0.5); }

.form .btn:hover {
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(11, 248, 63, 0.15);
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(5, 219, 76, 0.28); }

.form .modal-header {
    border-bottom: none; }
 

.form .modal-body, .form .modal-footer {
    font-weight: 400; }
    .blue-gradient {
        background: linear-gradient(360deg, #45cafc, #303f9f) !important;
    }
  .form-control{
      border: none;
      border-bottom: 1px solid grey;
  }


    </style>
</head>
<body >
<!-- Modal -->

<div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">

    <!--Content-->

    <div  class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
        <h3 class="modal-title w-100" id="myModalLabel"><strong>LogIn</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="">
        <div class="text-primary md-form mb-5">
            <label data-error="required"  for="Form-email1">E-mail</label>

            <input type="email" id="Form-email1"  class="form-control validate bg-transparent" required>
        </div>

        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Password</label>

            <input type="password" id="Form-pass1" class="form-control validate  bg-transparent">
          <p class="font-small text-dark d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1">
              Password?</a></p>
        </div>

        <div class="text-center mb-3">
          <button type="button" class="btn blue-gradient btn-block btn-rounded z-depth-1">LogIn</button>
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or LogIn
          with:</p>

          <!--icon-->
        <div class="row my-3 d-flex justify-content-center">
          
          <button type="button" class="btn btn-primary btn-rounded w-25 mr-md-3 z-depth-1"><i class="fab fa-facebook-f text-center"></i></button>
          
          <button type="button" class="btn btn-info btn-rounded w-25 ml-1 mr-md-3 z-depth-1"><i class="fab fa-twitter"></i></button>
          
          <button type="button" class="btn btn-danger btn-rounded w-25 ml-1 z-depth-1"><i class="fab fa-google-plus-g"></i></button>
        </div>
        </form>

      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="http://localhost/learning%20website%20PHP/registration.php" class="blue-text ml-1">
            Sign Up</a></p>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!-- Modal -->




<div class="text-center">
  <a href="" class="btn btn-default btn-rounded" data-toggle="modal" data-target="#ModalForm">
     Login </a>
</div>    

            



</body>
</html>