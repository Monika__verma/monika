<x-instructornavbar/>
<div class="container">
 <div class="row">
 @foreach($instructor_course_data as $course_data)
   <div class="col-md-6 mx-auto">
     <h2>Name:-{{$course_data->name}}</h2>

     @php
        $spec = explode(',',$course_data->course_specification);
        $requ = explode(',',$course_data->course_requirement);
        $cont = explode(',',$course_data->course_content);
        array_pop($spec);
        array_pop($requ);
        array_pop($cont);
        
     @endphp

     @foreach($spec as $specification)
     <h2>Specification:-{{$specification}}</h2>
     @endforeach 

     @foreach($requ as $requirement)
     <h2>Requirement:-{{$requirement}}</h2>
     @endforeach

     @foreach($cont as $content)
     <h2>Content:-{{$content}}</h2>
     @endforeach
     
     <h2>Description:-{{$course_data->course_description}}</h2>
     <h2>Price:-{{$course_data->course_price}}</h2>
     <h2>Vedio:-</h2>
     <video width='500' height='250' controls>
       <source src='{{asset("instructor_course_images/$course_data->image")}}'>
     </video>
     @foreach(explode(',',$course_data->image) as $img)
          <img src='{{asset("instructor_course_images/$img")}}' style='width:200px' alt="">

     @endforeach
   </div>
 @endforeach  
 </div>
    
    <!--    <div class="row">
            <div class="col-md-12 mx-auto px-0">
                <h1 class="text-center bg-info text-white">Created Courses</h1>
            </div>
            <div class="col-md-8 mx-auto mt-4">
                <label for="">Title</label>
                <p class="form-control">Php</p>
                <label for="">Category</label>
                <p class="form-control">IT</p>
                <label for="">What students will learn</label>
                <p class="form-control">Html , Css</p>
                <label for="">Course requirement</label>
                <p class="form-control">Basic of programming</p>
                <label for="">Course content</label>
                <p class="form-control">Php , laravel</p>
                <label for="">Description</label>
                <p class="form-control">Php full stack course</p>
                <label for="">Price</label>
                <p class="form-control">500</p>
                <input type="button" value="Edit/Manage" class="btn btn-info mt-3">
            </div>
        </div> -->
    </div>
</body>
</html>