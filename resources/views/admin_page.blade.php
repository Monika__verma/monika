<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('style/main.css')}}">
</head>
<body>
<div class="container bg-light">
    <div class="row">
        <div class="col-md-6 bg-dark text-white mx-auto">
            <h2>Add Courses</h2>
            <form action="{{url('add_course')}}" method="post" enctype='multipart/form-data'>
            @csrf()
            <label for="">Course Name</label>
              <input type="text" name="course_name" class='form-control'>
            <label for="">Course Description</label>
              <textarea type="text" name="course_description" class='form-control'></textarea> 
            <label for="">Course Image</label>
              <input type="file" name="course_image" class='form-control' id="">  
            <input type="submit" value="Add Course">  
            </form>
        </div>
    </div>
</div>
    
</body>
</html>