<x-instructornavbar/>
<head>
<link rel="stylesheet" href="{{asset('style/instructorprofile.css')}}">
</head>

    <div class="container mt-5">
        <h2>Profile & Settings</h2>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs mt-4" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Profile details</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Profile picture</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu2">Privacy setting</a>
            </li>
        </ul>
    
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- first profile page -->
            <div id="home" class="container tab-pane active">
                <div class="row">
                    <div class="col-md-6 mt-4 input-field">
                    <form action="">
                        <label for="">First Name</label>
                        <input type="text" placeholder="first name" class="form-control">
                        <label for="">Last Name</label>
                        <input type="text" placeholder="last name" class='form-control'>
                        <label for="">Headline</label>
                        <input type="text" placeholder="'Engineer at Udemy' or 'Architect'" class='form-control'>
                        <label for="">Biography</label>
                        <hr class="hr2">
                        <textarea class="form-control " name="message" cols="50" rows="5" placeholder="B  I"></textarea>
                        <p>Your biography should have at least 50 characters, links and coupon codes are not permitted.</p>
                        <label for="">Language</label>
                        <input type="text" placeholder="eg. Hindi,English" class='form-control'>
                    </form>
                    </div>
                    <div class="col-md-6 mt-4">
                        <form action="">
                            <label for="">Website</label>
                            <input type="text" placeholder="Url" class="form-control">
                            <label for="">Twitter</label><br>
                            <input type="text" class="twit" placeholder="http://www.twitter.com/" disabled ><input type="text" class="twit" placeholder="Username">
                            <label for="">Facebook</label><br>
                            <input type="text" class="twit" placeholder="http://www.facebook.com/" disabled><input type="text" class="twit" placeholder="Username">
                            <label for="">LinkedIn</label><br>
                            <input type="text" class="twit" placeholder="http://www.linkedin.com/" disabled><input type="text" class="twit" placeholder="Resource ID">
                            <label for="">Youtube</label><br>
                            <input type="text" class="twit" placeholder="http://www.youtube.com/" disabled><input type="text" class="twit" placeholder="Username">
                        </form>
                    </div>
                    <div class="col-md-12 mt-4" style="margin-left: 1100px ;">
                        <a href="" class="btn">Save</a>
                    </div>
                </div>
            </div>

            <!-- second profile page -->

            <div id="menu1" class="container tab-pane fade"><br>
                <div class="col-md-12 mt-5">
                    <h6>Image Preview</h6>
                    <p>Minimum 200x200 pixels, Maximum 6000x6000 pixels</p>
                    <img src="profile.jpg" href="#"><br>
                    <input type="text" class="profile-input" placeholder="No file selected" disabled><input type="text" class="profile-input" placeholder="Upload Image">
                </div>
                <div class="col-md-12 mt-4" style="margin-left: 340px ;">
                    <a href="" class="btn">Save</a>
                </div>
            </div>

            <!-- 3rd profile page -->

            <div id="menu2" class="container tab-pane fade mt-4"><br>
                <input type="checkbox"><a href="">Show your profile on search engines</a><br>
                <input type="checkbox"><a href="">Show courses you're taking on your profile page</a>
                <div class="col-md-12 mt-4" style="margin-left: 280px ;">
                    <a href="" class="btn">Save</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>