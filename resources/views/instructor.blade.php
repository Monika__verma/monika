@extends('navbar_footer')

@section('navbar')

@parent

@endsection


@section('main_content')

    <!-- Ist section -->

    <div class="container-fluid ist-section">
        <div class="row">
            <div class="img col-12">
                <div class="img-content">
                    <h1 class="display-4 m-0">Make a global impact</h1>
                    <p class="text-left">
                        Create an onsssline video course and earn money by teaching people around the world.
                    </p>
                    <button class="btn btn-info btn-lg" onclick="window.location.replace('instructordashboard.html')">Become An Instructor</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 2nd section -->

    <div class="container-fluid second-section">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-4">Discover Your Potential</h1>
            </div>
            <hr />
            <div class="col-md-4">
                <i class="fas fa-piggy-bank" style="font-size: 50px;"></i>
                <h4>Earn Money</h4>
                <p class="mx-auto">
                    Earn money every time a student purchases your course. Get paid monthly through PayPal or Payoneer, it’s your choice.
                </p>
            </div>
            <div class="col-md-4">
                <i class="fab fa-youtube" style="font-size: 50px;"></i>
                <h4>Inspire Students</h4>
                <p class="mx-auto">
                    Help people learn new skills, advance their careers, and explore their hobbies by sharing your knowledge.
                </p>
            </div>
            <div class="col-md-4">
                <i class="fas fa-thumbs-up" style="font-size: 50px;"></i>
                <h4>Join Our Community</h4>
                <p class="mx-auto">
                    Take advantage of our active community of instructors to help you through your course creation process.
                </p>
            </div>
        </div>
    </div>

    <!-- 3rd section -->

    <div class="container-fluid third-section">
        <div class="row">
            <div class="img col-12">
                <div class="img-content">
                    <h2>We're here to help</h2>
                    <hr />
                    <p class="mx-auto">
                        Our Instructor Support Team is here for you 24/7 to help you through your course creation needs. Use our Teaching Center, a resource center to help you through the process. Join Studio U and get peer-to-peer support from our engaged instructor community.
                        This community group is always on, always there, and always helpful.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- 4th section -->

    <div class="container-fluid fourth-section">
        <div class="row">
            <div class="col-md-12">
                <h2>Become an instructor today</h2>
                <p class="mx-auto">
                    Join the world's largest online learning marketplace.
                </p>
                <button class="btn btn-info">Get Started</button>
            </div>
        </div>
    </div>

    <!-- New Section  -->

    <section class="container new-section mt-5 pt-5 pb-5">
        <div class="row">
            <!-- navbar tabs -->
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a data-toggle="tab" class="nav-link active" href="#first">Plan Your Courses</a
              >
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#second"
                >Record Your Video</a
              >
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#third"
                >Build your community</a
              >
            </li>
          </ul>

          <div class="tab-content mt-5">
            <!-- First Tab -->
            <div id="first" class="tab-pane active">
              <div class="row">
                <div class="col-md-6 p-3">
                  <div class="left-content">
                    <h5>Plan Your Courses</h5>
                    <p>Get organised</p>
                    <p>
                      You start with your passion and knowledge. Then choose a
                      topic and plan your lectures in Google Docs, Microsoft
                      Excel, or your favorite notebook.
                    </p>
                    <p>
                      You get to teach the way you want — even create courses in
                      multiple languages and inspire more students.
                    </p>
                    <p>How we help you</p>
                    <p>
                      Udemy offers free courses on how to build your own course,
                      complete with worksheets and real-world examples. Plus,
                      our instructor dashboard and curriculum pages help keep
                      you on track.
                    </p>
                  </div>
                </div>
                <!-- Second Row of Row -->
                <div class="col-md-6">
                  <div class="right-content img">
                    <img
                      src="https://www.udemy.com/staticx/udemy/images/teaching/v6/Sign_up_illustration_v2.svg"
                      class="d-block mx-auto mt-5"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>

            <!-- Second Tabs -->
            <div id="second" class="tab-pane fade">
              <div class="row">
                <div class="col-md-6 p-3">
                  <div class="left-content">
                    <h5>Record Your Video</h5>
                    <p>Lights, camera, action</p>
                    <p>
                      Got a smartphone or a DSLR? Add a microphone and you’re
                      ready to film your first course from your home or wherever
                      you happen to be.
                    </p>
                    <p>
                      Camera shy? No Worries. Use screencasting software, like
                      Camtasia, to screencast demos.
                    </p>
                    <p>How we help you</p>
                    <p>
                      Our support team is available to help and offers free
                      review of your videos. And our online instructor community
                      is here to offer advice and inspiration.
                    </p>
                  </div>
                </div>
                <!-- Second Row of Row -->
                <div class="col-md-6">
                  <div class="right-content img">
                    <img
                      src="https://www.udemy.com/staticx/udemy/images/teaching/v6/Create_course_illustration_v3.svg"
                      class="d-block mx-auto mt-5"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>

            <!-- Third Tabs -->
            <div id="third" class="tab-pane fade">
              <div class="row">
                <div class="col-md-6 p-3">
                  <div class="left-content">
                    <h5>Build your community</h5>
                    <p>Reach out</p>
                    <p>
                      Create quizzes, exercises, and assignments to build
                      interactivity. Write welcome messages for your students
                      and answer their questions. Because Udemy students don’t
                      just watch, they do.
                    </p>
                    <p>How we help you</p>
                    <p>
                      We provide all the tools for messaging, Q&As, course
                      announcements, and quizzes, all in one one place.
                    </p>
                  </div>
                </div>
                <!-- Second Row of Row -->
                <div class="col-md-6">
                  <div class="right-content img">
                    <img
                      src="https://www.udemy.com/staticx/udemy/images/teaching/v6/Start_sharing_illustration_v2.svg"
                      class="d-block mx-auto mt-5"
                      alt=""
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    @endsection



@section('footer')

@parent

@endsection