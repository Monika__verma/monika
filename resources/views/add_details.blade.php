<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Details</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <style>
        /* .container-fluid
        {
            padding-left: unset !important;
            padding-right: unset !important;
        } */
        .title_error{
            display:none;
        }
    </style>
</head>
<body style="background-color: #f2f3f5;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mx-auto px-0">
                <h1 class="text-center bg-info text-white">Detailed information about course</h1>
                    <p class="text-center">The descriptions you write here will help students decide if your course is the one for them.</p>
                <form action="{{url('create_course_form')}}" method='post' enctype='multipart/form-data'>
                @csrf()
                    <input type="hidden" name="user_email" value='{{Session::get("login_email")}}' id="">
                    <input type="hidden" name="user_id" value='{{Session::get("login_user_id")}}' id="">
                    <div class="col-md-8 col-lg-8 mx-auto mt-4">
                        <label class="">How about a working title?</label>
                        <input type="text" name='title' placeholder="e.g. Learn Photoshop CS6 from scratch" class=" form-control form1 " id="title" required>
                        <div class="title_error h4">
                        </div>
                        <span class="text-center   font-weight-normal" style='font-size:12px'>It's ok if you can't think of a good title now. you can change it later.</span>
                    </div>          
                    <div class="col-md-8 col-lg-8 mx-auto mt-4">
                        <lable class="text-center">What category best fit the knowledge you will share?</lable>
                        <select id="category" name='category' class="form-control" required>
                        @foreach($courses_data as $data)
                            <option value='{{$data->name}}'>{{$data->name}}</option>
                        @endforeach    
                        </select>
                        <span class="text-center  mt-4 font-weight-normal" style='font-size:12px'>If you are not sure about the right category, you can change it later.</span>
                    </div>        
                    <div class="col-md-8 mx-auto mt-4">
                        <label for="">What will students learn in your course?</label>
                        <div class='student_increment'>
                            <input type="text" name='case_specification[]' class="form-control" placeholder="Example-HTML, CSS" required>
                        </div>
                        <input type="button" value="Add an answer" class="btn btn-info mt-2 add_student_learn">
                        <div class="student_clone " style='display:none'>
                            <div class='input-group student_input-group'>
                                <input type="text" name='case_specification[]' class="form-control" placeholder="Example-HTML, CSS">
                                <button class="btn btn-danger student_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Course requirements</label>
                        <div class='requirement_increment'>
                            <input type="text" name='course_requirement[]' class="form-control" placeholder="Example-Basics of programming" required>
                        </div>
                        <input type="button" value="Add an answer" class="btn btn-info mt-2 add_requirement_btn">
                        <div class="requirement_clone " style='display:none'>
                            <div class='input-group requirement_input-group'>
                                <input type="text" name='course_requirement[]' class="form-control" placeholder="Example-Basics of programming">
                                <button class="btn btn-danger requirement_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Course content</label>
                        <div class='course_content_increment'>
                            <input type="text" name='course_content[]' class="form-control" placeholder="Example-Basic of HTML" required>
                        </div>
                        <input type="button" value="Add an answer" class="btn btn-info mt-2 add_course_content">
                        <div class="course_content_clone" style='display:none'>
                            <div class='input-group course_content_input-group'>
                                <input type="text" name='course_content[]' class="form-control" placeholder="Example-Basic of HTML">
                                <button class="btn btn-danger course_content_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Description</label>
                        <div class='description_increment'>
                        <input type="text" name='course_description' class="form-control" placeholder="Add description" required>
                        </div>
                        
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Price</label>
                        <div class='price_increment'>
                            <input type="number" name='course_price' class="form-control" placeholder="Add price" required>
                        </div>
                        <!-- <input type="button" value="Add" class="btn btn-info mt-2 add_price"> -->
                        <!-- <div class="price_clone" style='display:none'>
                            <div class='input-group price_input-group'>
                                <input type="number" name='course_price[]' class="form-control" placeholder="Add price">
                                <button class="btn btn-danger price_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Image</label>
                        <div class='image_increment'>
                            <input type="file" name='course_image[]' class="form-control" placeholder="Add price" required>
                        </div>
                        <input type="button" value="Add" class="btn btn-info mt-2 add_image"> 
                         <div class="image_clone" style='display:none'>
                            <div class='input-group image_input-group'>
                                <input type="file" name='course_image[]' class="form-control" placeholder="Add price">
                                <button class="btn btn-danger image_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Notes/Documents</label>
                        <div class='notes_increment'>
                            <input type="file" name='course_notes[]' class="form-control" placeholder="Add price" required>
                        </div>
                        <input type="button" value="Add" class="btn btn-info mt-2 add_notes"> 
                         <div class="notes_clone" style='display:none'>
                            <div class='input-group notes_input-group'>
                                <input type="number" name='course_notes[]' class="form-control" placeholder="Add price">
                                <button class="btn btn-danger notes_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-8 mx-auto mt-3">
                        <label for="">Upload video</label>
                        <div class='video_increment'>
                            <input type="file" name='course_video[]' class="form-control" required>
                            
                        </div>
                        <input type="button" value="Add video" class="btn btn-info mt-2 add_video">
                        <div class="video_clone" style='display:none'>
                            <div class='input-group video_input-group'>
                                <input type="file" name='course_video[]' class="form-control">
                                <button class="btn btn-danger video_delete_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="Save" class="btn btn-info mt-5 d-block w-25 mx-auto">
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".add_student_learn").click(function(){ 
                var lsthmtl = $(".student_clone").html();
                // alert(lsthmtl)
                $(".student_increment").after(lsthmtl);
            });
            $("body").on("click",".student_delete_btn",function(){ 
                // alert($(this))  
                $(this).parents(".student_input-group").remove();
            });
        });
        $(document).ready(function() {
            $(".add_requirement_btn").click(function(){ 
                var lsthmtl = $(".requirement_clone").html();
                // alert(lsthmtl)
                $(".requirement_increment").after(lsthmtl);
            });
            $("body").on("click",".requirement_delete_btn",function(){ 
                // alert($(this))  
                $(this).parents(".requirement_input-group").remove();
            });
        });
        $(document).ready(function() {
            $(".add_course_content").click(function(){ 
                var lsthmtl = $(".course_content_clone").html();
                // alert(lsthmtl)
                $(".course_content_increment").after(lsthmtl);
            });
            $("body").on("click",".course_content_delete_btn",function(){ 
                // alert($(this))  
                $(this).parents(".course_content_input-group").remove();
            });
        });
        $(document).ready(function() {
            $(".add_description").click(function(){ 
                var lsthmtl = $(".description_clone").html();
                // alert(lsthmtl)
                $(".description_increment").after(lsthmtl);
            });
            $("body").on("click",".description_delete_btn",function(){ 
                // alert($(this))  
                $(this).parents(".description_input-group").remove();
            });
        });
         $(document).ready(function() {
            $(".add_image").click(function(){ 
                var lsthmtl = $(".image_clone").html();
                 // alert(lsthmtl)
                 $(".image_increment").after(lsthmtl);
             });
             $("body").on("click",".image_delete_btn",function(){ 
                 // alert($(this))  
                $(this).parents(".image_input-group").remove();
            });
         });
         $(document).ready(function() {
            $(".add_notes").click(function(){ 
                var lsthmtl = $(".notes_clone").html();
                 // alert(lsthmtl)
                 $(".notes_increment").after(lsthmtl);
             });
             $("body").on("click",".notes_delete_btn",function(){ 
                 // alert($(this))  
                $(this).parents(".notes_input-group").remove();
            });
         });

        $(document).ready(function() {
            $(".add_video").click(function(){ 
                var lsthmtl = $(".video_clone").html();
                // alert(lsthmtl)
                $(".video_increment").after(lsthmtl);
            });
            $("body").on("click",".video_delete_btn",function(){ 
                // alert($(this))  
                $(this).parents(".video_input-group").remove();
            });
        });
    </script>

    <script>
    $(function(){
        $('#title').keyup(function(){
            let title = $(this).val();
            $.ajax({
                url:'{{url("check_title")}}',
                type:'get',
                data:{title,title},
                success:function(data)
                {
                    if(data == 'title_exists')
                    {
                        $('#title').addClass('border').addClass('border-danger');
                        $('.title_error').css('display','block');
                        $('.title_error').removeClass('bg-success').addClass('bg-danger').addClass('text-white');
                        $('.title_error').html('title already exists');
                    }else{
                        $('.title_error').removeClass('bg-danger').addClass('bg-success').addClass('text-white');
                        $('.title_error').css('display','block');
                        $('.title_error').html('you can use this title');
                        $('#title').addClass('border').addClass('border-success');
                    }
                }
            })
        })
    })
    </script>

</body>
</html>