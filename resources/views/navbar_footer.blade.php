<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Page</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('style/main.css')}}">
    <style>
        .active a{
        background:green;
        padding:10px;
        color:black !important;
    }
        .dropdown:hover .dropdown-menu{
          display:block;
        }

    </style>
</head>

<body>
    @section('navbar')
<!--Menu Bar  -->
<div class="menu">
<nav class="navbar navbar-expand-lg navbar-light" id="menubar">
            <a href="{{url('/')}}" class="navbar-brand text-white">Techacademics</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#hit">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="hit">
                <ul class="navbar-nav mx-auto">
                    <li class="{{'/' == request()->path() ? 'active':''}} nav-item">
                        <a href="{{url('/')}}" class="nav-link active ">Home</a>
                    </li>
                    <li class="nav-item">
                        <div class="search-bar">
                            <form action=" ">
                                <input type="text">
                                <i class="fa fa-search search-icon"></i>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Courses</a>
                        <div class="dropdown-menu">
                             <x-datashare/>   

                         </div>
                    </li>
                    <li class="{{'instructor' == request()->path() ? 'active':''}} nav-item">
                        <a href="{{url('instructor')}}" class="nav-link">Tutors</a>
                    </li>
                    <li class="{{'viewmycourses' == request()->path() ? 'active':''}} nav-item">
                        <a href="{{url('viewmycourses')}}" class="nav-link">My Courses</a>
                    </li>
                    <li class="{{'cart_page' == request()->path() ? 'active':''}} nav-item">
                        <a href="{{url('cart_page')}}" class="nav-link ">Cart</a>
                    </li>
                    <li class="{{'aboutus' == request()->path() ? 'active':''}} nav-item">
                        <a href="{{url('aboutus')}}" class="nav-link ">About Us</a>
                    </li>
                    @if(!Session::has('login_email'))
                    <li class="nav-item">
                        <a href="#" data-toggle="modal" data-target="#registerform" class="nav-link ">Register</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-toggle="modal" data-target="#loginform" class="nav-link ">Login</a>
                    </li>
                    @endif
                </ul>
                @if(Session::has('login_email'))
                <ul class="navbar-nav ml-auto">
                    <div class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                          My Profile
                        </a>
                        <div class="dropdown-menu profile_dropdown">
                            <a class="dropdown-item" href="viewmycourses.html">My Courses</a>
                            <a class="dropdown-item" href="cart_page.html">My Cart</a>
                            <a class="dropdown-item" href="profile.html">Edit Profile</a>
                            <a class="dropdown-item" href="changepassword.html">Change Password</a>
                            <a class="dropdown-item" href="#">Logout</a>
                        </div>
                    </div>
                </ul>
                @endif
                <!-- <ul class="navbar-nav ml-auto login ">
                    <li class="nav-item ">
                        <a href="# " class="nav-link ">Login</a>
                    </li>
                    <li class="nav-item ">
                        <a href="# " class="nav-link ">Sign Up</a>
                    </li>
                </ul> -->
            </div>
        </nav>
    </div>
<!--menu bar end -->
@show



@yield('main_content')



@section('footer')
<!-- Footer -->
<footer class="footer container-fluid ">
        <div class="upper-footer ">
            <div class="row ">
                <div class="col-md-3 mx-auto mt-4 ">
                    <p>Our mission is to provide a free, world-class education to anyone,anywhere.</p>
                    <!-- <img src="bby.jpg " alt=" " class="img-fluid d-block img mb-2 "> -->

                </div>
                <div class="col-md-3 mx-auto mt-4 ">
                    <a href="">ABOUT US</a>
                    <p><a>News</a></p>
                    <p><a>Impact</a></p>
                    <p><a>Our team</a></p>
                    <p><a>Our leadership</a></p>
                    <p><a>Careers</a></p>
                    <p><a>Internship</a></p>
                </div>
                <div class="col-md-3 mt-4 ">
                    <p class="font-weight-bold ">CONTACT US</p>
                    <p><span class="font-weight-bold ">Address :</span> Hath of it fly signs bear be one blessed after</p>
                    <p><a>Help center</a></p>
                    <p><a>Share your story</a></p>

                    <p><a><span class="font-weight-bold ">Phone :</span> 600540987</a></p>
                    <p><a><span class="font-weight-bold ">Email :</span>info@learning.com </a></p>
                </div>
            </div>
        </div>
        <hr>
        <div class="icons ">
            <div class="row ">
                <div class="col-md-6 list mx-auto ">
                    <ul>
                        <li>Terms of use</li>
                        <li class="ml-5 ">Privacy Policy</li>
                    </ul>
                </div>
                <div class="col-md-6 ">
                    <i class="fab fa-facebook "></i>
                    <i class="fab fa-twitter "></i>
                    <i class="fab fa-instagram "></i>
                </div>
            </div>
        </div>
        <hr>
        <div class="lower-footer text-center ">
            Copyright @E-Learning All rights reserved
        </div>
</footer>

<!-- Javascript's -->
<script src="Javascript/index.js "></script>
</body>

</html>
@show




  
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signin</title>
    
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <script src="{{asset('jquery-3.4.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">

    <style>
.form .font-small {
    font-size: 1; }

.form .z-depth-1 {
    box-shadow: 0 2px 5px 0 rgba(14, 130, 231, 0.5), 0 4px 12px 0 rgb(185, 196, 230,0.5);
    box-shadow: 0 2px 5px 0 rgba(4, 104, 190, 0.5), 0 4px 12px 0 rgba(6, 59, 219, 0.5); }

.form .btn:hover {
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(11, 248, 63, 0.15);
    box-shadow: 0 5px 11px 0 rgba(5, 219, 76, 0.28), 0 4px 15px 0 rgba(5, 219, 76, 0.28); }

.form .modal-header {
    border-bottom: none; }
 

.form .modal-body, .form .modal-footer {
    font-weight: 400; }
    .blue-gradient {
        background: linear-gradient(360deg, #45cafc, #303f9f) !important;
    }
  .form-control{
      border: none;
      border-bottom: 1px solid grey;
  }


    </style>
</head>
<body >
<!-- register Modal -->

<div class="modal fade" id="registerform" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <!--Content-->

    <div class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
        @if($errors->any())
          <div class="alert alert-danger">
            <ul class="list-group">
              @foreach($errors->all() as $error)
                <li class="list-group-item">{{$error}}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <h3 class="modal-title w-100" id="myModalLabel"><strong>SignIn</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="{{url('register_form')}}" method="post">
        @csrf()
        

        <div class="text-primary md-form mb-3">
            <label data-error="required"  for="Form-email1">E-mail</label>

            <input type="email" id="Form-email1" name="email" class="form-control validate bg-transparent"required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
        </div>

        
        <div class="text-center mb-3">
          <input type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1" name="submit" value="Rrgister">
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or SignIn
          with:</p>

          <!--icon-->
        <div class="row my-3 d-flex justify-content-center">
          
          <!--<button type="button" class="btn btn-primary btn-rounded w-25 mr-md-3 z-depth-1"><i class="fab fa-facebook-f text-center"></i></button>
          
          <button type="button" class="btn btn-info btn-rounded w-25 ml-1 mr-md-3 z-depth-1"><i class="fab fa-twitter"></i></button> -->
          <a href ="{{url('redirect')}}">
          <button type="button" class="btn btn-danger btn-rounded  ml-1 z-depth-1" style='width:100px'><i class="fab fa-google-plus-g"></i></button></a>
        </div>
        </form>
        
      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="#" class="blue-text ml-1">
            Sign Up</a></p>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!--register Modal end -->


<!--login  Modal -->

<div class="modal fade" id="loginform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <!--Content-->

    <div  class="modal-content  form">

      <!--Header-->

      <div class="modal-header text-center">
        <h3 class="modal-title w-100" id="myModalLabel"><strong>LogIn</strong></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <!--Body-->

      <div class="modal-body mx-4">

        <!--Body-->
        <form action="{{url('login_form')}}" method='post'>
        @csrf()
        <div class="text-primary md-form mb-5">
            <label data-error="required"  for="Form-email1">E-mail</label>

            <input type="email" id="Form-email1" name='email' class="form-control validate bg-transparent" required>
        </div>

        <div class=" text-primary md-form pb-3">
            <label data-error="required"  for="Form-pass1"> Password</label>

            <input type="password" id="Form-pass1" name='password' class="form-control validate  bg-transparent">
          <p class="font-small text-dark d-flex justify-content-end">Forgot <a href="#" class="blue-text ml-1">
              Password?</a></p>
        </div>

        <div class="text-center mb-3">
          <button type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1">LogIn</button>
        </div>
        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or LogIn
          with:</p>

          <!--icon-->
       <!-- <div class="row my-3 d-flex justify-content-center">
          
          <button type="button" class="btn btn-primary btn-rounded w-25 mr-md-3 z-depth-1"><i class="fab fa-facebook-f text-center"></i></button>
          
          <button type="button" class="btn btn-info btn-rounded w-25 ml-1 mr-md-3 z-depth-1"><i class="fab fa-twitter"></i></button>
          
          <button type="button" class="btn btn-danger btn-rounded w-25 ml-1 z-depth-1"><i class="fab fa-google-plus-g"></i></button>
        </div>-->
        </form>

      </div>
      <!--Footer-->
      <div class="modal-footer mx-5 pt-3 mb-1">
        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="http://localhost/learning%20website%20PHP/registration.php" class="blue-text ml-1">
            Sign Up</a></p>
      </div>
    </div>
    <!--Content-->
  </div>
</div>
<!-- login Modal end -->




 

            



</body>
</html>


<script>
 // $(function(){
   // $('#ModalForm').modal('show');
  //})

</script> 

</body>
</html>