<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<h2>Your email verification link is here. <a href="{{url('verification_email',[$t = $token])}}">Click Here</a></h2>
    
</body>
</html>