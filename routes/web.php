<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome');
//});


//common page

Route::get('/','index_page@index');
Route::get('single_course/{id}','index_page@show');
//Route::get('registration_page','registration_page@index');
Route::post('register_form','registration_page@store');

Route::get('registration_complete_view',function(){
    return view('registration_complete_page');
});

Route::post('register_complete_form','registration_page@update');

Route::get('verification_email/{token}','registration_page@verify_email_fn');

Route::post('login_form','registration_page@login_form');

Route::get('/logout','registration_page@logout');

Route::get('instructor',function(){
    return view('instructor');
})->middleware('student_check');

Route::get('viewmore/{id}','index_page@viewsinglecourse');


//common pages end

Route::get('course_category',function(){
    
});

//user pages
Route::get('cart_page',function(){
    return view('cart_page');
})->middleware('student_check');

Route::get('viewmycourses',function(){
    return view('viewmycourses');
})->middleware('student_check');

Route::get('profile',function(){
    return view('profile');
});

Route::get('changepassword',function(){
    return view('changepassword');
});

Route::post('add_to_cart','user_page@store');



//Instructor pages


Route::get('instructor_dashboard',function(){
    return view('instructordashboard');
})->middleware('instructor_check');

Route::get('course_create','instructor_page@course_create');

    Route::get('instructorprofile',function(){ 
        return view('instructorprofile'); 
    });

Route::get('show_course','instructor_page@show_courses');

Route::get('add_details',function(){
     return view('add_details');
     });


Route::post('create_course_form','instructor_page@store');

Route::get('check_title','instructor_page@check_title');


Route::get('course',function(){
    return view('course');
});

Route::get('coursecategory',function(){
    return view('coursecategory');
});

Route::get('admindashboard',function(){
    return view('admindashboard');
});

Route::get('demotry',function(){
    return view('demotry');
});

Route::get('expandpage',function(){
    return view('expandpage');
});

Route::get('feedback',function(){
    return view('feedback');
});

Route::get('knowledge',function(){
    return view('knowledge');
});



Route::get('/aboutus','about_page@index');

Route::get('/navbar','about_page@navbar');



// Admin pages
Route::get('admin','admin_page@index');


Route::post('add_course','admin_page@store');



//google route
Route::get('/redirect', 'SocialAuthGoogleControllerController@redirect');
Route::get('/google_callback', 'SocialAuthGoogleControllerController@callback');

